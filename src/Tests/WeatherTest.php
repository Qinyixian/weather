<?php

namespace Qinii\Weather\Tests;

use Qinii\Weather\Weather;
use GuzzleHttp\Client;
use GuzzleHttp\ClientInterface;
use GuzzleHttp\Psr7\Response;
use Mockery\Matcher\AnyArgs;
use PHPUnit\Framework\TestCase;
use Qinii\Weather\Exceptions\InvalidArgumentException;
use Qinii\Weather\Exceptions\HttpException;

class WeatherTest extends TestCase
{
    public function testGetWeatherWithInvalidType()
    {
        $w = new Weather('mock-key');
        $this->expectException(InvalidArgumentException::class);
        $this->expectExceptionMessage('Invalid type value(base/all)');

        $w->getWeather('西安市','error');
        $this->fail('Failed to assert getWeather throw');
    }

    public function testGetWeatherWithInvalidFormat() {
        $w = new Weather('mock-key'); // 断言会抛出此异常类
        $this->expectException(InvalidArgumentException::class);
        $this->expectExceptionMessage('Invalid response format:array'); 
        $w->getWeather('西安市', 'base', 'array');
        $this->fail('Failed to assert getWeather throw exception');
    }


    public function testGetWeather()
    {
        // xml
        $response = new Response(200, [], '<hello>content</hello>');
        $client = \Mockery::mock(Client::class);
        $client->allows()->get('https://restapi.amap.com/v3/weather/weatherInfo', [
            'query' => [
                'key' => 'mock-key',
                'city' => '西安',
                'extensions' => 'all',
                'output' => 'xml',
            ],
        ])->andReturn($response);

        $w = \Mockery::mock(Weather::class, ['mock-key'])->makePartial();
        $w->allows()->getHttpClient()->andReturn($client);

        $this->assertSame('<hello>content</hello>', $w->getWeather('西安', 'all', 'xml'));

        // json
        $response = new Response(200, [], '{"success": true}');
        $client = \Mockery::mock(Client::class);
        $client->allows()->get('https://restapi.amap.com/v3/weather/weatherInfo', [
            'query' => [
                'key' => 'mock-key',
                'city' => '西安',
                'output' => 'json',
                'extensions' => 'base',
            ],
        ])->andReturn($response);

        $w = \Mockery::mock(Weather::class, ['mock-key'])->makePartial();
        $w->allows()->getHttpClient()->andReturn($client);

        $this->assertSame(['success' => true], $w->getWeather('西安')); 
    }

    public function testGetWeatherWithGuzzleRuntimeException(Type $var = null)
    {
       $client = \Mockery::mock(Client::class);
       $client->allows()->get(new AnyArgs())->andThrow(new \Exception('request timeout'));

       $w = \Mockery::mock(Weather::class,['mock-key'])->makePartial();
       $w->allows()->getHttpClient()->andReturn($client);

       $this->expectException(HttpException::class);
       $this->expectExceptionMessage('request timeout');

       $w->getWeather('西安');
    }

    public function testGetHttpClient()
    {
        $w = new Weather('mock-key');

        $this->assertInstanceOf(Client::class,$w->getHttpClient());
    }

    public function testSetGuzzleOptions()
    {
        $w = new Weather('mock-key');
        $this->assertNull($w->getHttpClient()->getConfig('timeout'));
        $w->setGuzzleOptions(['timeout' => 5000]);
        $this->assertSame(5000,$w->getHttpClient()->getConfig('timeout'));
    }
    
    
}