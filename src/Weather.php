<?php

namespace Qinii\Weather;

use GuzzleHttp\Client;
use Qinii\Weather\Exceptions\InvalidArgumentException;
use Qinii\Weather\Exceptions\HttpException;

class Weather
{
	//f98e07a5ba16d7d9a1882066c31a1fe9
	protected $key;
	protected $guzzleOptions = [];

	public function __construct(string $key)
	{
		$this->key = $key;	
	}

	public function getHttpClient()
	{
		return new Client($this->guzzleOptions);
	}

	public function setGuzzleOptions(array $options)
	{
		$this->guzzleOptions = $options;
	}

	public function getWeather($city,$type = 'base',$format = 'json')
	{
		$url = 'https://restapi.amap.com/v3/weather/weatherInfo';

		if(!\in_array(\strtolower($format),['xml','json'])){
			throw new InvalidArgumentException('Invalid response format:'.$format);
		}
		if(!\in_array(\strtolower($type),['base','all'])){
			throw new InvalidArgumentException('Invalid type value(base/all)');
		}
		$query = array_filter([
			'key'  => $this->key,
			'city' => $city,
			'output' => $format,
			'extensions' => $type, 
		]);

		try{
			$response = $this->getHttpClient()->Get($url,[
				'query' => $query,
			])->getBody()->getContents();
			
			return 'json' === $format ? \json_decode($response) : $response;
		}catch(\Exception $e){
			throw new HttpException($e->getMessage(),$e->getCode(),$e);
		}
	}
}